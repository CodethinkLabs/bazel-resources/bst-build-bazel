resolved = [
    {
        "original_rule_class": "local_repository",
        "original_attributes": {
            "name": "bazel_tools",
            "path": "/home/tom/.cache/bazel/_bazel_tom/install/33dc6d1fd2d274bf22d1119cd66dd0e6/_embedded_binaries/embedded_tools"
        },
        "native": "local_repository(name = \"bazel_tools\", path = __embedded_dir__ + \"/\" + \"embedded_tools\")"
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'bazel_toolchains' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:387:1",
        "original_attributes": {
            "name": "bazel_toolchains",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/bazel-toolchains/archive/0.28.3.tar.gz",
                "https://github.com/bazelbuild/bazel-toolchains/archive/0.28.3.tar.gz"
            ],
            "sha256": "d8c2f20deb2f6143bac792d210db1a4872102d81529fe0ea3476c1696addd7ff",
            "strip_prefix": "bazel-toolchains-0.28.3"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "3415da436d561dc8d23ab4327f9cb2282d7ba29215db43c952a7b5d537e20e04",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/bazel-toolchains/archive/0.28.3.tar.gz",
                        "https://github.com/bazelbuild/bazel-toolchains/archive/0.28.3.tar.gz"
                    ],
                    "sha256": "d8c2f20deb2f6143bac792d210db1a4872102d81529fe0ea3476c1696addd7ff",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "bazel-toolchains-0.28.3",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "bazel_toolchains"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'io_bazel_skydoc' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:444:1",
        "original_attributes": {
            "name": "io_bazel_skydoc",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/skydoc/archive/c7bbde2950769aac9a99364b0926230060a3ce04.tar.gz",
                "https://github.com/bazelbuild/skydoc/archive/c7bbde2950769aac9a99364b0926230060a3ce04.tar.gz"
            ],
            "sha256": "e6a76586b264f30679688f65f7e71ac112d1446681010a13bf22d9ca071f34b7",
            "strip_prefix": "skydoc-c7bbde2950769aac9a99364b0926230060a3ce04"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "61da5b88212be2772224bb86e7318932b1bc55c923a51933c64d55cddbb11037",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/skydoc/archive/c7bbde2950769aac9a99364b0926230060a3ce04.tar.gz",
                        "https://github.com/bazelbuild/skydoc/archive/c7bbde2950769aac9a99364b0926230060a3ce04.tar.gz"
                    ],
                    "sha256": "e6a76586b264f30679688f65f7e71ac112d1446681010a13bf22d9ca071f34b7",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "skydoc-c7bbde2950769aac9a99364b0926230060a3ce04",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "io_bazel_skydoc"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'io_bazel_rules_sass' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:609:1",
        "original_attributes": {
            "name": "io_bazel_rules_sass",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/rules_sass/archive/8ccf4f1c351928b55d5dddf3672e3667f6978d60.tar.gz",
                "https://github.com/bazelbuild/rules_sass/archive/8ccf4f1c351928b55d5dddf3672e3667f6978d60.tar.gz"
            ],
            "sha256": "d868ce50d592ef4aad7dec4dd32ae68d2151261913450fac8390b3fd474bb898",
            "strip_prefix": "rules_sass-8ccf4f1c351928b55d5dddf3672e3667f6978d60"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "c3c36cd74338514dfcbf84352ef4f91a4a6502a6facfe54bd76d55ca858c34f1",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/rules_sass/archive/8ccf4f1c351928b55d5dddf3672e3667f6978d60.tar.gz",
                        "https://github.com/bazelbuild/rules_sass/archive/8ccf4f1c351928b55d5dddf3672e3667f6978d60.tar.gz"
                    ],
                    "sha256": "d868ce50d592ef4aad7dec4dd32ae68d2151261913450fac8390b3fd474bb898",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "rules_sass-8ccf4f1c351928b55d5dddf3672e3667f6978d60",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "io_bazel_rules_sass"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'build_bazel_rules_nodejs' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:619:1",
        "original_attributes": {
            "name": "build_bazel_rules_nodejs",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/rules_nodejs/archive/0.16.2.zip",
                "https://github.com/bazelbuild/rules_nodejs/archive/0.16.2.zip"
            ],
            "sha256": "9b72bb0aea72d7cbcfc82a01b1e25bf3d85f791e790ddec16c65e2d906382ee0",
            "strip_prefix": "rules_nodejs-0.16.2"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "ad0d9bfa712cc7adee6dca334ebaefcc0c160c99b22608307c989d78dd1ea689",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/rules_nodejs/archive/0.16.2.zip",
                        "https://github.com/bazelbuild/rules_nodejs/archive/0.16.2.zip"
                    ],
                    "sha256": "9b72bb0aea72d7cbcfc82a01b1e25bf3d85f791e790ddec16c65e2d906382ee0",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "rules_nodejs-0.16.2",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "build_bazel_rules_nodejs"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'bazel_skylib' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:431:1",
        "original_attributes": {
            "name": "bazel_skylib",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/bazel-skylib/archive/f83cb8dd6f5658bc574ccd873e25197055265d1c.tar.gz",
                "https://github.com/bazelbuild/bazel-skylib/archive/f83cb8dd6f5658bc574ccd873e25197055265d1c.tar.gz"
            ],
            "sha256": "ba5d15ca230efca96320085d8e4d58da826d1f81b444ef8afccd8b23e0799b52",
            "strip_prefix": "bazel-skylib-f83cb8dd6f5658bc574ccd873e25197055265d1c"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "bcbe8795189e6710cb7ae8896a4d5248feb0b31df0efb322e8373861703a6e8a",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/bazel-skylib/archive/f83cb8dd6f5658bc574ccd873e25197055265d1c.tar.gz",
                        "https://github.com/bazelbuild/bazel-skylib/archive/f83cb8dd6f5658bc574ccd873e25197055265d1c.tar.gz"
                    ],
                    "sha256": "ba5d15ca230efca96320085d8e4d58da826d1f81b444ef8afccd8b23e0799b52",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "bazel-skylib-f83cb8dd6f5658bc574ccd873e25197055265d1c",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "bazel_skylib"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'rules_pkg' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:690:1",
        "original_attributes": {
            "name": "rules_pkg",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/rules_pkg/rules_pkg-0.2.0.tar.gz",
                "https://github.com/bazelbuild/rules_pkg/releases/download/0.2.0/rules_pkg-0.2.0.tar.gz"
            ],
            "sha256": "5bdc04987af79bd27bc5b00fe30f59a858f77ffa0bd2d8143d5b31ad8b1bd71c"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "9188a460bbcb474e6a9a48a635e8b5d6205e71c9b3b8224554c556273835a048",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/rules_pkg/rules_pkg-0.2.0.tar.gz",
                        "https://github.com/bazelbuild/rules_pkg/releases/download/0.2.0/rules_pkg-0.2.0.tar.gz"
                    ],
                    "sha256": "5bdc04987af79bd27bc5b00fe30f59a858f77ffa0bd2d8143d5b31ad8b1bd71c",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "rules_pkg"
                }
            }
        ]
    },
    {
        "original_rule_class": "//src/main/res:winsdk_configure.bzl%winsdk_configure",
        "definition_information": "Call stack for the definition of repository 'local_config_winsdk' which is a winsdk_configure (rule definition at /home/tom/Downloads/bazel/src/main/res/winsdk_configure.bzl:169:20):\n - /home/tom/Downloads/bazel/WORKSPACE:706:1",
        "original_attributes": {
            "name": "local_config_winsdk"
        },
        "repositories": [
            {
                "rule_class": "//src/main/res:winsdk_configure.bzl%winsdk_configure",
                "output_tree_hash": "a8351670de192176d6c5ca114bbf1a52bb8270881f4648ffe488ae7ac8b3b1ff",
                "attributes": {
                    "name": "local_config_winsdk"
                }
            }
        ]
    },
    {
        "original_rule_class": "local_config_platform",
        "original_attributes": {
            "name": "local_config_platform"
        },
        "native": "local_config_platform(name = 'local_config_platform')"
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'platforms' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:653:1",
        "original_attributes": {
            "name": "platforms",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/platforms/archive/441afe1bfdadd6236988e9cac159df6b5a9f5a98.zip",
                "https://github.com/bazelbuild/platforms/archive/441afe1bfdadd6236988e9cac159df6b5a9f5a98.zip"
            ],
            "sha256": "a07fe5e75964361885db725039c2ba673f0ee0313d971ae4f50c9b18cd28b0b5",
            "strip_prefix": "platforms-441afe1bfdadd6236988e9cac159df6b5a9f5a98"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "3f09ade14d0260f51eeee677488da48148304648a7afc09cec387a43184a15ae",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/platforms/archive/441afe1bfdadd6236988e9cac159df6b5a9f5a98.zip",
                        "https://github.com/bazelbuild/platforms/archive/441afe1bfdadd6236988e9cac159df6b5a9f5a98.zip"
                    ],
                    "sha256": "a07fe5e75964361885db725039c2ba673f0ee0313d971ae4f50c9b18cd28b0b5",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "platforms-441afe1bfdadd6236988e9cac159df6b5a9f5a98",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "platforms"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'rules_cc' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:454:1",
        "original_attributes": {
            "name": "rules_cc",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/rules_cc/archive/0d5f3f2768c6ca2faca0079a997a97ce22997a0c.zip",
                "https://github.com/bazelbuild/rules_cc/archive/0d5f3f2768c6ca2faca0079a997a97ce22997a0c.zip"
            ],
            "sha256": "36fa66d4d49debd71d05fba55c1353b522e8caef4a20f8080a3d17cdda001d89",
            "strip_prefix": "rules_cc-0d5f3f2768c6ca2faca0079a997a97ce22997a0c"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "7f487a4ba08b793dbab12253aa825d1877305e0e165c51a2e8213ce80590c6f6",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/rules_cc/archive/0d5f3f2768c6ca2faca0079a997a97ce22997a0c.zip",
                        "https://github.com/bazelbuild/rules_cc/archive/0d5f3f2768c6ca2faca0079a997a97ce22997a0c.zip"
                    ],
                    "sha256": "36fa66d4d49debd71d05fba55c1353b522e8caef4a20f8080a3d17cdda001d89",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "rules_cc-0d5f3f2768c6ca2faca0079a997a97ce22997a0c",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "rules_cc"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/sh:sh_configure.bzl%sh_config",
        "definition_information": "Call stack for the definition of repository 'local_config_sh' which is a sh_config (rule definition at /tmp/bazel1/external/bazel_tools/tools/sh/sh_configure.bzl:72:13):\n - /tmp/bazel1/external/bazel_tools/tools/sh/sh_configure.bzl:83:5\n - /DEFAULT.WORKSPACE.SUFFIX:391:1",
        "original_attributes": {
            "name": "local_config_sh"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/sh:sh_configure.bzl%sh_config",
                "output_tree_hash": "7bf5ba89669bcdf4526f821bc9f1f9f49409ae9c61c4e8f21c9f17e06c475127",
                "attributes": {
                    "name": "local_config_sh"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/cpp:cc_configure.bzl%cc_autoconf_toolchains",
        "definition_information": "Call stack for the definition of repository 'local_config_cc_toolchains' which is a cc_autoconf_toolchains (rule definition at /tmp/bazel1/external/bazel_tools/tools/cpp/cc_configure.bzl:79:26):\n - /tmp/bazel1/external/bazel_tools/tools/cpp/cc_configure.bzl:178:5\n - /DEFAULT.WORKSPACE.SUFFIX:385:1",
        "original_attributes": {
            "name": "local_config_cc_toolchains"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/cpp:cc_configure.bzl%cc_autoconf_toolchains",
                "output_tree_hash": "4da0f8a3ddc1facf0ff4a5195b161aefa1630a14fa928ad04691971e5dd2380b",
                "attributes": {
                    "name": "local_config_cc_toolchains"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'rules_java' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:464:1",
        "original_attributes": {
            "name": "rules_java",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/rules_java/archive/7cf3cefd652008d0a64a419c34c13bdca6c8f178.zip",
                "https://github.com/bazelbuild/rules_java/archive/7cf3cefd652008d0a64a419c34c13bdca6c8f178.zip"
            ],
            "sha256": "bc81f1ba47ef5cc68ad32225c3d0e70b8c6f6077663835438da8d5733f917598",
            "strip_prefix": "rules_java-7cf3cefd652008d0a64a419c34c13bdca6c8f178"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "00a0f1231dacff0b0cea3886200e0158c67a3600068275da14120f5434f83b5e",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/rules_java/archive/7cf3cefd652008d0a64a419c34c13bdca6c8f178.zip",
                        "https://github.com/bazelbuild/rules_java/archive/7cf3cefd652008d0a64a419c34c13bdca6c8f178.zip"
                    ],
                    "sha256": "bc81f1ba47ef5cc68ad32225c3d0e70b8c6f6077663835438da8d5733f917598",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "rules_java-7cf3cefd652008d0a64a419c34c13bdca6c8f178",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "rules_java"
                }
            }
        ]
    },
    {
        "original_rule_class": "local_repository",
        "original_attributes": {
            "name": "io_bazel",
            "path": "."
        },
        "native": "local_repository(name = \"io_bazel\", path = \".\")"
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'rules_proto' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /home/tom/Downloads/bazel/WORKSPACE:474:1",
        "original_attributes": {
            "name": "rules_proto",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz",
                "https://github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz"
            ],
            "sha256": "602e7161d9195e50246177e7c55b2f39950a9cf7366f74ed5f22fd45750cd208",
            "strip_prefix": "rules_proto-97d8af4dc474595af3900dd85cb3a29ad28cc313"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "4f74a84e9684fc73c48656a9a6c60bdfaed2cabdf5dc7d21da7ae2ee78c7efa3",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz",
                        "https://github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz"
                    ],
                    "sha256": "602e7161d9195e50246177e7c55b2f39950a9cf7366f74ed5f22fd45750cd208",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "rules_proto-97d8af4dc474595af3900dd85cb3a29ad28cc313",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "rules_proto"
                }
            }
        ]
    },
    {
        "original_rule_class": "new_local_repository",
        "original_attributes": {
            "name": "rules_python",
            "path": "./third_party/rules_python",
            "build_file": "//third_party/rules_python:BUILD"
        },
        "native": "new_local_repository(name = \"rules_python\", path = \"./third_party/rules_python\", build_file = \"//third_party/rules_python:BUILD\")"
    },
    {
        "original_rule_class": "@bazel_tools//tools/cpp:cc_configure.bzl%cc_autoconf",
        "definition_information": "Call stack for the definition of repository 'local_config_cc' which is a cc_autoconf (rule definition at /tmp/bazel1/external/bazel_tools/tools/cpp/cc_configure.bzl:143:15):\n - /tmp/bazel1/external/bazel_tools/tools/cpp/cc_configure.bzl:179:5\n - /DEFAULT.WORKSPACE.SUFFIX:385:1",
        "original_attributes": {
            "name": "local_config_cc"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/cpp:cc_configure.bzl%cc_autoconf",
                "output_tree_hash": "f0126c88e2c8fc785c0996d9ba3aaf8868c784f999c00abb47afd51bdfd408f7",
                "attributes": {
                    "name": "local_config_cc"
                }
            }
        ]
    },
    {
        "original_rule_class": "new_local_repository",
        "original_attributes": {
            "name": "com_google_protobuf",
            "path": "./third_party/protobuf/3.6.1/",
            "build_file": "./third_party/protobuf/3.6.1/BUILD"
        },
        "native": "new_local_repository(name = \"com_google_protobuf\", path = \"./third_party/protobuf/3.6.1/\", build_file = \"./third_party/protobuf/3.6.1/BUILD\")"
    },
    {
        "original_rule_class": "new_local_repository",
        "original_attributes": {
            "name": "local_jdk",
            "path": "/usr/lib/jvm/java-11-openjdk",
            "build_file": "/home/tom/.cache/bazel/_bazel_tom/install/33dc6d1fd2d274bf22d1119cd66dd0e6/_embedded_binaries/jdk.BUILD"
        },
        "native": "new_local_repository(name = \"local_jdk\", path = \"/usr/lib/jvm/java-11-openjdk\", build_file = __embedded_dir__ + \"/\" + \"jdk.BUILD\")"
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_file",
        "definition_information": "Call stack for the definition of repository 'openjdk_linux_minimal' which is a http_file (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:372:13):\n - /home/tom/Downloads/bazel/WORKSPACE:256:1",
        "original_attributes": {
            "name": "openjdk_linux_minimal",
            "downloaded_file_path": "zulu-linux-minimal.tar.gz",
            "sha256": "5123bc8dd21886761d1fd9ca0fb1898b3372d7243064a070ec81ca9c9d1a6791",
            "urls": [
                "https://mirror.bazel.build/openjdk/azul-zulu11.29.3-ca-jdk11.0.2/zulu11.29.3-ca-jdk11.0.2-linux_x64-minimal-524ae2ca2a782c9f15e00f08bd35b3f8ceacbd7f-1556011926.tar.gz"
            ]
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_file",
                "output_tree_hash": "5b7f845782a3d7f86c8ac0c67a222bbb5ee8db059ac26ae106716833b7dc748e",
                "attributes": {
                    "executable": False,
                    "downloaded_file_path": "zulu-linux-minimal.tar.gz",
                    "sha256": "5123bc8dd21886761d1fd9ca0fb1898b3372d7243064a070ec81ca9c9d1a6791",
                    "urls": [
                        "https://mirror.bazel.build/openjdk/azul-zulu11.29.3-ca-jdk11.0.2/zulu11.29.3-ca-jdk11.0.2-linux_x64-minimal-524ae2ca2a782c9f15e00f08bd35b3f8ceacbd7f-1556011926.tar.gz"
                    ],
                    "netrc": "",
                    "name": "openjdk_linux_minimal"
                }
            }
        ]
    },
    {
        "original_rule_class": "local_repository",
        "original_attributes": {
            "name": "googleapis",
            "path": "./third_party/googleapis/"
        },
        "native": "local_repository(name = \"googleapis\", path = \"./third_party/googleapis/\")"
    },
    {
        "original_rule_class": "local_repository",
        "original_attributes": {
            "name": "remoteapis",
            "path": "./third_party/remoteapis/"
        },
        "native": "local_repository(name = \"remoteapis\", path = \"./third_party/remoteapis/\")"
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'remote_java_tools_linux' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/utils.bzl:205:9\n - /DEFAULT.WORKSPACE.SUFFIX:260:1",
        "original_attributes": {
            "name": "remote_java_tools_linux",
            "urls": [
                "https://mirror.bazel.build/bazel_java_tools/releases/javac11/v6.1/java_tools_javac11_linux-v6.1.zip",
                "https://github.com/bazelbuild/java_tools/releases/download/javac11-v6.1/java_tools_javac11_linux-v6.1.zip"
            ],
            "sha256": "12f7940ed0bc4c2e82238951cdf19b4179c7dcc361d16fe40fe4266538fb4ac6"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "73d8e7b1ff4623400c633a2e2c2a00756d62324e36c9f6edc580f50b42bbb06e",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/bazel_java_tools/releases/javac11/v6.1/java_tools_javac11_linux-v6.1.zip",
                        "https://github.com/bazelbuild/java_tools/releases/download/javac11-v6.1/java_tools_javac11_linux-v6.1.zip"
                    ],
                    "sha256": "12f7940ed0bc4c2e82238951cdf19b4179c7dcc361d16fe40fe4266538fb4ac6",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "remote_java_tools_linux"
                }
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Call stack for the definition of repository 'remotejdk11_linux' which is a http_archive (rule definition at /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/http.bzl:292:16):\n - /tmp/bazel1/external/bazel_tools/tools/build_defs/repo/utils.bzl:205:9\n - /DEFAULT.WORKSPACE.SUFFIX:216:1",
        "original_attributes": {
            "name": "remotejdk11_linux",
            "urls": [
                "https://mirror.bazel.build/openjdk/azul-zulu11.2.3-jdk11.0.1/zulu11.2.3-jdk11.0.1-linux_x64.tar.gz"
            ],
            "sha256": "232b1c3511f0d26e92582b7c3cc363be7ac633e371854ca2f2e9f2b50eb72a75",
            "strip_prefix": "zulu11.2.3-jdk11.0.1-linux_x64",
            "build_file": "@local_jdk//:BUILD.bazel"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "output_tree_hash": "41eeb95428accd781c6818d658697a95762c0d7d894e78d13dfb8bc72726e377",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/openjdk/azul-zulu11.2.3-jdk11.0.1/zulu11.2.3-jdk11.0.1-linux_x64.tar.gz"
                    ],
                    "sha256": "232b1c3511f0d26e92582b7c3cc363be7ac633e371854ca2f2e9f2b50eb72a75",
                    "netrc": "",
                    "canonical_id": "",
                    "strip_prefix": "zulu11.2.3-jdk11.0.1-linux_x64",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file": "@local_jdk//:BUILD.bazel",
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "remotejdk11_linux"
                }
            }
        ]
    }
]