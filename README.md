# bst-build-bazel

This is an example project to build bazel[0] and an example bazel project inside
of BuildStream[1], acting as the testing ground for a set of BuildStream
plugins for using bazel.

As an example we build bazel using bazel inside BuildStream, and build the
first example C++ project from the bazel examples repo[2].

# Dependencies

Building this project requires:
* BuildStream[3] \(At least 1.91.2-71-g054375f31bdb183fc7a7100c0e90a8b48348c260\)
  - Thus also buildbox-casd[4]
* bst-plugins-experimental (latest master)

# Using

To build the whole stack use

```shell
bst build bazel-example.bst
```

At the moment there is no cache available with freedesktop-sdk built with
BuildStream 1.91.2, and as such building this means building the whole stack
from scratch. This could take a *very* long time depending on how powerful your
hardware is, so beware!

You can test the build of `bazel-example.bst` worked by running

```shell
bst shell bazel-example.bst hello-world
```

which should print out a basic hello world statement.
